var sajamApp = angular.module("sajamApp", ['ngRoute']);

sajamApp.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/',{
        templateUrl: '/static/app/html/partial/zadaci.html'
    }).when('/zadaci/edit/:id',{
        templateUrl: '/static/app/html/partial/zadatak-edit.html'
    }).when('/filtriranje',{
        templateUrl: '/static/app/html/partial/filtriranje.html'
    }).when('/projekti',{
        templateUrl: '/static/app/html/partial/projekti.html'
    }).otherwise({
        redirectTo: '/'
    });
}]);

sajamApp.controller("zadaciCtrl", function($scope, $http, $location){

    $scope.base_url_zadaci = "/api/zadaci";
    $scope.base_url_zaposleni = "/api/zaposleni";
    $scope.base_url_tipovi = "api/tipovi";


    $scope.pageNum = 0;
    $scope.totalPages = 0;
    $scope.pom = false;

    $scope.zadaci = [];
    $scope.zaposleni = [];
    $scope.tipovi = [];

    $scope.noviZadatak = {};
    $scope.noviZadatak.naziv = "";
    $scope.noviZadatak.opis = "";
    $scope.noviZadatak.prioritet = "";
    $scope.noviZadatak.rokZavrsetka = "";
    $scope.noviZadatak.zaposleniId = "";
    $scope.noviZadatak.tipId = "";
    $scope.noviZadatak.tipNaziv = "";


    // $scope.tip = {};
    // $scope.tip.naziv = "";

    var getZaposleni = function(){

        $http.get($scope.base_url_zaposleni)
            .then(function success(data){
                $scope.zaposleni = data.data;
                console.log(data.data);
                //alert("Uspesno dobavljeni sajmovi");
            });

    };

    var getTip = function(){
        $http.get($scope.base_url_tipovi)
            .then(function success(data){
                $scope.tipovi = data.data;
                console.log(data.data);
                //alert("Uspesno dobavljeni sajmovi");
            });
    }

    var getZadaci = function(){

        var config = {params: {}};

        config.params.pageNum = $scope.pageNum;
        $http.get($scope.base_url_zadaci, config)
            .then(function success(data){
                console.log(data.data);
                $scope.zadaci = data.data;
                $scope.totalPages = data.headers("totalPages");
            });
    };

    getTip();
    getZaposleni();
    getZadaci();
    

    $scope.nazad = function(){
        if($scope.pageNum > 0) {
            $scope.pageNum = $scope.pageNum - 1;
            getZadaci();
        }
    };

    $scope.napred = function(){
        if($scope.pageNum < $scope.totalPages - 1){
            $scope.pageNum = $scope.pageNum + 1;
            getZadaci();
        }
    };

    $scope.dodaj = function(){
        console.log($scope.noviZadatak);

        if($scope.pom == false){
            $http.post($scope.base_url_zadaci, $scope.noviZadatak)
            .then(function success(data){
                console.log(data.data);
                alert("Uspesno dodat štand u bazu.");
                getZadaci();
            });
        }else{
             $http.put($scope.base_url_zadaci + "/" + $scope.noviZadatak.id, $scope.noviZadatak)
            .then(function success(data){
                alert("Uspešno izmenjen objekat!");
                getZadaci();
            },function error(data,status){
                console.log(data,data);          
            });
        }
        $scope.pom = false;
    };


    $scope.izmeni = function(id){
        $location.path('/zadaci/edit/' + id);
    }

    $scope.obrisi = function(id){
        $http.delete($scope.base_url_zadaci + "/" + id).then(
            function success (data,status){
                alert("Uspesno brisanje zadatka sa id: " + id);
                getZadaci();
            });
    }

    $scope.naFiltriranje = function(){
         $location.path('/filtriranje');
    }

    $scope.izmeniOvde = function(id){
         $http.get($scope.base_url_zadaci + "/" + id)
            .then(function success(data){
               $scope.noviZadatak = data.data;
                $scope.pom = true;
                // alert(data.data.sajamId + " je id sajma za ovaj stand");
            });

    }
});

sajamApp.controller("editZadaciCtrl", function($scope, $http, $routeParams, $location){

    $scope.base_url_zadaci = "/api/zadaci";
    $scope.base_url_zaposleni = "/api/zaposleni";
    $scope.base_url_tipovi = "api/tipovi";

    $scope.zaposleni = [];
    $scope.tipovi = [];

    $scope.stariZadatak = null;

    var getStariZadatak = function(){

        $http.get($scope.base_url_zadaci + "/" + $routeParams.id)
            .then(function success(data){
               $scope.stariZadatak = data.data;
                
                // alert(data.data.sajamId + " je id sajma za ovaj stand");
            });

    }

    var getZaposleni = function(){
        getTip();
        $http.get($scope.base_url_zaposleni)
            .then(function success(data){
                $scope.zaposleni = data.data;
                console.log(data.data);
                getStariZadatak();//Ispravio sam, i sada se odavde poziva! Da bih bio siguran da postoje sajmovi pre nego sto se izvrse provere u ng-selected!
            });

    };

    var getTip = function(){
        $http.get($scope.base_url_tipovi)
            .then(function success(data){
                $scope.tipovi = data.data;
                console.log(data.data);
                //alert("Uspesno dobavljeni sajmovi");
            });
    }

    getZaposleni();

    
    
    $scope.izmeni = function(){
         // console.log($scope.stariStand);
        $http.put($scope.base_url_zadaci + "/" + $scope.stariZadatak.id, $scope.stariZadatak)
            .then(function success(data){
                alert("Uspešno izmenjen objekat!");
                console.log($scope.stariZadatak.id);
                $location.path("/");
            },function error(data,status){
                console.log(data,data);          
            });
    }
});

sajamApp.controller("filterCtrl", function($scope, $http, $routeParams, $location){

    $scope.base_url_zadaci = "/api/zadaci";
    $scope.base_url_zaposleni = "/api/zaposleni";


    $scope.pageNum = 0;
    $scope.totalPages = 0;
    $scope.pom = 0;

    $scope.zadaci = [];
    $scope.zaposleni = [];

    $scope.noviZadatak = {};
    $scope.noviZadatak.naziv = "";
    $scope.noviZadatak.opis = "";
    $scope.noviZadatak.prioritet = "";
    $scope.noviZadatak.rokZavrsetka = "";
    $scope.noviZadatak.zaposleniId = "";

    $scope.trazeniZadatak = {};
    $scope.trazeniZadatak.minP = "";
    $scope.trazeniZadatak.maxP = "";
    $scope.trazeniZadatak.naziv = "";

    var getZadaci = function(){

        var config = {params: {}};

        config.params.pageNum = $scope.pageNum;

        if($scope.trazeniZadatak.minP != ""){
            config.params.minP = $scope.trazeniZadatak.minP;
        }

        if($scope.trazeniZadatak.maxP != ""){
            config.params.maxP = $scope.trazeniZadatak.maxP;
        }

        if($scope.trazeniZadatak.naziv != ""){
            config.params.naziv = $scope.trazeniZadatak.naziv;
        }

        if($scope.pom != 0){
            config.params.minP = 5;
            config.params.maxP = 5;
            config.params.naziv = "";
        }

       


        $http.get($scope.base_url_zadaci, config)
            .then(function success(data){
                console.log(data.data);
                $scope.zadaci = data.data;
                $scope.totalPages = data.headers('totalPages');
                //alert("Radi dobavljanje standova");

            });

            $scope.pom = 0;
    };

    var getZaposleni = function(){

        $http.get($scope.base_url_zaposleni)
            .then(function success(data){
                $scope.zaposleni = data.data;
                console.log(data.data);
                //alert("Uspesno dobavljeni sajmovi");
            });

    };

    getZadaci();
    getZaposleni();

    $scope.nazad = function(){
        if($scope.pageNum > 0) {
            $scope.pageNum = $scope.pageNum - 1;
            getZadaci();
        }
    };

    $scope.napred = function(){
        if($scope.pageNum < $scope.totalPages - 1){
            $scope.pageNum = $scope.pageNum + 1;
            getZadaci();
        }
    };

    $scope.dodaj = function(){
        console.log($scope.noviZadatak);
        $http.post($scope.base_url_zadaci, $scope.noviZadatak)
            .then(function success(data){
                console.log(data.data);
                alert("Uspesno dodat štand u bazu.");
                getZadaci();
            });
    };

    $scope.trazi = function () {
        $scope.pageNum = 0;
        getZadaci();
    }

    $scope.izmeni = function(id){
        $location.path('/zadaci/edit/' + id);
    }

    $scope.obrisi = function(id){
        $http.delete($scope.base_url_zadaci + "/" + id).then(
            function success (data,status){
                alert("Uspesno brisanje zadatka sa id: " + id);
                getZadaci();
            });
    }

    $scope.naZadatke = function(){
        $location.path('/');
    }

 $scope.najprioritetniji = function () {
        $scope.pageNum = 0;
        $scope.pom = 1;
        getZadaci();
    }

});

sajamApp.controller('projektiCtrl', function($scope,$http){

   $scope.base_url_zadaci = "api/zadaci/all";

   $scope.zadaci = [];

   var getZadaci = function(){

    $http.get($scope.base_url_zadaci).then(
        function success(data, status){
            console.log(data.data);
            $scope.zadaci = data.data;
        });
   }

   getZadaci();


});

sajamApp.controller('loginCtrl', function($scope, $http, $routeParams, $location){

	   $scope.base_url_login = "api/login";

	  $scope.user.username = "";
	  $scope.user.password = "";

	  var login = function(){
		  $http.post($scope.base_url_login, $scope.user)
          .then(function success(data){
              console.log(data.data);  
              $location.path("/");
          },function error(data,status){
              console.log(data,data);          
          });  
	  }
	 

	});








