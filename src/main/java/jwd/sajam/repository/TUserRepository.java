package jwd.sajam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import jwd.sajam.model.TUser;

public interface TUserRepository extends JpaRepository<TUser, Integer>{

	TUser findByUsername(String username);
	
	TUser findByUsernameAndPassword(String username, String password);

}
