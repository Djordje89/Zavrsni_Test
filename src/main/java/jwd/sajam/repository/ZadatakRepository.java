package jwd.sajam.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import jwd.sajam.model.Zadatak;

@Repository
public interface ZadatakRepository extends JpaRepository<Zadatak, Long> {

	Page<Zadatak> findByZaposleniId(Long zaposleniId, Pageable pageRequest);
	
	@Query("SELECT z FROM Zadatak z WHERE "
			+ "(:minP IS NULL OR z.prioritet >= :minP  ) AND "
			+ "(:maxP IS NULL OR z.prioritet <= :maxP) AND "
			+ "(:naziv IS NULL or z.naziv like :naziv ) "
			)
	Page<Zadatak> pretraga(
			@Param("minP") Integer min, 
			@Param("maxP") Integer max,
			@Param("naziv") String naziv, 
			Pageable pageable);
	
}
