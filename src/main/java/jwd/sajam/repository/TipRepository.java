package jwd.sajam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jwd.sajam.model.Tip;

@Repository
public interface TipRepository extends JpaRepository<Tip, Long> {

}
