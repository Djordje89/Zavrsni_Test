package jwd.sajam.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import jwd.sajam.model.TUser;
import jwd.sajam.service.UserService;
import jwd.sajam.support.TUserToTUserDTO;
import jwd.sajam.web.dto.TUserDTO;

@RestController
@RequestMapping(value = "/api/login")
public class ApiLoginController {

	@Autowired
	UserService userService;
	@Autowired
	private TUserToTUserDTO toTUserDTO;
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<TUserDTO> get(
			@RequestBody TUserDTO userDTO){
		
		
		TUser user = userService.findByUsernameAndPassword(userDTO.getUsername(), userDTO.getPassword());
		
		return new ResponseEntity<>(toTUserDTO.convert(user),
				HttpStatus.OK);
	}
	
	
	
	
	
}
