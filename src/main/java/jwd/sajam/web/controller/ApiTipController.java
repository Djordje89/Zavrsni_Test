package jwd.sajam.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import jwd.sajam.service.TipService;
import jwd.sajam.support.TipToTipDTO;
import jwd.sajam.web.dto.TipDTO;

@RestController
@RequestMapping(value="/api/tipovi")
public class ApiTipController {

	@Autowired
	private TipService tipService;
	@Autowired
	private TipToTipDTO toDTO;
	
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<TipDTO>> get(){
		return new ResponseEntity<>(
				toDTO.convert(tipService.findAll()),
				HttpStatus.OK);
	}
}
