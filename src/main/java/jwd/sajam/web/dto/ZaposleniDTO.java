package jwd.sajam.web.dto;


public class ZaposleniDTO {

	
	private Long id;
	private String ime;
	private String prezime;
	private String pozicija;
	private int brojDodeljenihZadataka;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public String getPozicija() {
		return pozicija;
	}
	public void setPozicija(String pozicija) {
		this.pozicija = pozicija;
	}
	public int getBrojDodeljenihZadataka() {
		return brojDodeljenihZadataka;
	}
	public void setBrojDodeljenihZadataka(int brojDodeljenihZadataka) {
		this.brojDodeljenihZadataka = brojDodeljenihZadataka;
	}
	
	
	
}
