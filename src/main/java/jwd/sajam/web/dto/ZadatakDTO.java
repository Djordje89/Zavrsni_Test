package jwd.sajam.web.dto;


public class ZadatakDTO {

	private Long id;

	private String naziv;

	private String opis;

	private int prioritet;

	private String rokZavrsetka;
	
	private Long zaposleniId;
	
	private String zaposleniIme;
	
	private String zaposleniPrezime;
	
	private Long tipId;
	
	private String tipNaziv;

	public Long getTipId() {
		return tipId;
	}

	public void setTipId(Long tipId) {
		this.tipId = tipId;
	}

	public String getTipNaziv() {
		return tipNaziv;
	}

	public void setTipNaziv(String tipNaziv) {
		this.tipNaziv = tipNaziv;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public int getPrioritet() {
		return prioritet;
	}

	public void setPrioritet(int prioritet) {
		this.prioritet = prioritet;
	}

	public String getRokZavrsetka() {
		return rokZavrsetka;
	}

	public void setRokZavrsetka(String rokZavrsetka) {
		this.rokZavrsetka = rokZavrsetka;
	}

	public Long getZaposleniId() {
		return zaposleniId;
	}

	public void setZaposleniId(Long zaposleniId) {
		this.zaposleniId = zaposleniId;
	}

	public String getZaposleniIme() {
		return zaposleniIme;
	}

	public void setZaposleniIme(String zaposleniIme) {
		this.zaposleniIme = zaposleniIme;
	}

	public String getZaposleniPrezime() {
		return zaposleniPrezime;
	}

	public void setZaposleniPrezime(String zaposleniPrezime) {
		this.zaposleniPrezime = zaposleniPrezime;
	}
	
	
	
}
