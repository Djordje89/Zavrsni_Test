package jwd.sajam;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jwd.sajam.model.TUser;
import jwd.sajam.model.Tip;
import jwd.sajam.model.Zadatak;
import jwd.sajam.model.Zaposleni;
import jwd.sajam.repository.TUserRepository;
import jwd.sajam.service.TipService;
import jwd.sajam.service.ZadatakService;
import jwd.sajam.service.ZaposleniService;

@Component
public class TestData {
	@Autowired
	private ZadatakService zadatakService;
	@Autowired
	private ZaposleniService zaposleniService;
	@Autowired
	private TipService tipService;
	@Autowired
	private TUserRepository tUserRepository;

	@PostConstruct
	public void init() {
		
		Tip t1 = new Tip();
		t1.setNaziv("Modifikovanje");
		tipService.save(t1);
		
		Tip t2 = new Tip();
		t2.setNaziv("kodiranje");
		tipService.save(t2);
		
		
		Zaposleni z1 = new Zaposleni();
		z1.setIme("Djordje");
		z1.setPrezime("Tomovic");
		z1.setPozicija("junior");
		z1.setBrojDodeljenihZadataka(3);
		zaposleniService.save(z1);
		
		Zaposleni z2 = new Zaposleni();
		z2.setIme("Katarina");
		z2.setPrezime("MIkic");
		z2.setPozicija("senior");
		z2.setBrojDodeljenihZadataka(6);
		zaposleniService.save(z2);
		
		Zaposleni z3 = new Zaposleni();
		z3.setIme("Pera");
		z3.setPrezime("Peric");
		z3.setPozicija("junior");
		z3.setBrojDodeljenihZadataka(1);
		zaposleniService.save(z3);
		
		
		
		
		Zadatak zad1 = new Zadatak();
		zad1.setNaziv("Plan programa");
		zad1.setOpis("Pravljenje metoda");
		zad1.setPrioritet(1);
		zad1.setRokZavrsetka("01.04.2017");
		zad1.setZaposleni(z3);
		zad1.setTip(t2);
		zadatakService.save(zad1);
		
		Zadatak zad2 = new Zadatak();
		zad2.setNaziv("KOnstrukcija metoda");
		zad2.setOpis("konstrukcija metoda");
		zad2.setPrioritet(3);
		zad2.setRokZavrsetka("11.04.2017");
		zad2.setZaposleni(z2);
		zad2.setTip(t1);
		zadatakService.save(zad2);
		
		Zadatak zad3 = new Zadatak();
		zad3.setNaziv("konekcija");
		zad3.setOpis("Povezivanje sa bazom");
		zad3.setPrioritet(2);
		zad3.setRokZavrsetka("12.07.2017");
		zad3.setZaposleni(z3);
		zad3.setTip(t2);
		zadatakService.save(zad3);
		
		Zadatak zad4 = new Zadatak();
		zad4.setNaziv("testiranje");
		zad4.setOpis("testiranje");
		zad4.setPrioritet(6);
		zad4.setRokZavrsetka("21.11.2017");
		zad4.setZaposleni(z1);
		zad4.setTip(t1);
		zadatakService.save(zad4);
		
		Zadatak zad5 = new Zadatak();
		zad5.setNaziv("Dokumentacija");
		zad5.setOpis("spisak rada");
		zad5.setPrioritet(4);
		zad5.setRokZavrsetka("11.07.2017");
		zad5.setZaposleni(z3);
		zad5.setTip(t2);
		zadatakService.save(zad5);
		
		Zadatak zad6 = new Zadatak();
		zad6.setNaziv("produkcija");
		zad6.setOpis("pustanje u rad");
		zad6.setPrioritet(1);
		zad6.setRokZavrsetka("12.11.2017");
		zad6.setZaposleni(z1);
		zad6.setTip(t2);
		zadatakService.save(zad6);
		
		Zadatak zad7 = new Zadatak();
		zad7.setNaziv("prodaja");
		zad7.setOpis("razgovor sa klijentima");
		zad7.setPrioritet(5);
		zad7.setRokZavrsetka("21.01.2018");
		zad7.setZaposleni(z2);
		zad7.setTip(t2);
		zadatakService.save(zad7);
	
		TUser user1 = new TUser();
		user1.setFirstName("Marko");
		user1.setLastName("Markovic");
		user1.setUsername("marko");
		user1.setPassword("$2a$06$g.JVjxtP729PDeAc06DN..ogF6Ke1FBNG9hA2aHsYxo64IVIGYeO6"); //BCrypt(password1)
		user1.setRole("ROLE_USER");
		tUserRepository.save(user1);
		
		TUser user2 = new TUser();
		user2.setFirstName("Djordje");
		user2.setLastName("Tomovic");
		user2.setUsername("djordje");
		user2.setPassword("$2a$06$g.JVjxtP729PDeAc06DN..ogF6Ke1FBNG9hA2aHsYxo64IVIGYeO6"); //BCrypt(password1)
		user2.setRole("ROLE_ADMIN");
		tUserRepository.save(user2);
		
		
		
	}
}