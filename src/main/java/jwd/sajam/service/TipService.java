package jwd.sajam.service;

import java.util.List;

import jwd.sajam.model.Tip;

public interface TipService {

	List<Tip> findAll();
	Tip findOne(Long id);
	void save(Tip tip);
	void remove(Long id);
}
