package jwd.sajam.service;

import java.util.List;

import jwd.sajam.model.Zaposleni;

public interface ZaposleniService {

	List<Zaposleni> findAll();
	Zaposleni findOne(Long id);
	void save(Zaposleni zaposleni);
	void remove(Long id);
}
