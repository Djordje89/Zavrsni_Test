package jwd.sajam.service;

import jwd.sajam.model.TUser;

public interface UserService {

	
	TUser findByUsernameAndPassword(String username, String password);
}
