package jwd.sajam.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;

import jwd.sajam.model.Zadatak;

public interface ZadatakService {

	Page<Zadatak> findAll(int pageNum);
	List<Zadatak> findAll();
	Zadatak findOne(Long id);
	void save(Zadatak zadatak);
	void remove(Long id);
	Page<Zadatak> findByZaposleniId(int pageNum, Long zaposleniId);
	
	Page<Zadatak> pretraga(
			@Param("minP") Integer min, 
			@Param("maxP") Integer max,
			@Param("naziv") String naziv, 
			int page);
	
	
}
