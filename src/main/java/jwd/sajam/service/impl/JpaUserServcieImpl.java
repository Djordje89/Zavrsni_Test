package jwd.sajam.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jwd.sajam.model.TUser;
import jwd.sajam.repository.TUserRepository;
import jwd.sajam.service.UserService;

@Service
@Transactional
public class JpaUserServcieImpl implements UserService {

	@Autowired
	TUserRepository tUserRepository;
	
	@Override
	public TUser findByUsernameAndPassword(String username, String password) {
		
		return tUserRepository.findByUsernameAndPassword(username, password);
	}

}
