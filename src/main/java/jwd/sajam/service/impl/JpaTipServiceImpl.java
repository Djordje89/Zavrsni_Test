package jwd.sajam.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jwd.sajam.model.Tip;
import jwd.sajam.repository.TipRepository;
import jwd.sajam.service.TipService;

@Service
@Transactional
public class JpaTipServiceImpl implements TipService {

	@Autowired
	private TipRepository tipRepository;
	
	
	@Override
	public List<Tip> findAll() {
		return tipRepository.findAll();
	}

	@Override
	public Tip findOne(Long id) {
		return tipRepository.findOne(id);
	}

	@Override
	public void save(Tip tip) {
		tipRepository.save(tip);
	}

	@Override
	public void remove(Long id) {
		tipRepository.delete(id);
	}

}
