package jwd.sajam.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import jwd.sajam.model.Zadatak;
import jwd.sajam.repository.ZadatakRepository;
import jwd.sajam.service.ZadatakService;

@Service
@Transactional
public class JpaZadatakServiceImp implements ZadatakService {

	@Autowired
	private ZadatakRepository zadatakRepository;

	
	
	@Override
	public Page<Zadatak> findAll(int pageNum) {
		return zadatakRepository.findAll(new PageRequest(pageNum, 5));
	}

	@Override
	public Zadatak findOne(Long id) {
		return zadatakRepository.findOne(id);
	}

	@Override
	public void save(Zadatak zadatak) {
	zadatakRepository.save(zadatak);
	}

	@Override
	public void remove(Long id) {
		zadatakRepository.delete(id);

	}
	
	@Override
	public Page<Zadatak> findByZaposleniId(int pageNum, Long zadatakId) {
		
		return zadatakRepository.findByZaposleniId(zadatakId, new PageRequest(pageNum, 5));
	}

	@Override
	public Page<Zadatak> pretraga(Integer min, Integer max,String naziv, int page) {
		if(naziv != null ){
			naziv = "%" + naziv + "%";
		}
		return zadatakRepository.pretraga(min, max,naziv, new PageRequest(page, 5));
	}

	@Override
	public List<Zadatak> findAll() {
		return zadatakRepository.findAll();
	}
	

}
