package jwd.sajam.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.sajam.model.Zadatak;
import jwd.sajam.web.dto.ZadatakDTO;

@Component
public class ZadatakTOZadatakDTO implements Converter<Zadatak, ZadatakDTO> {

	@Override
	public ZadatakDTO convert(Zadatak source) {
		
		ZadatakDTO dto = new ZadatakDTO();
		dto.setId(source.getId());
		dto.setNaziv(source.getNaziv());
		dto.setOpis(source.getOpis());
		dto.setPrioritet(source.getPrioritet());
		dto.setRokZavrsetka(source.getRokZavrsetka());
		dto.setZaposleniId(source.getZaposleni().getId());
		dto.setZaposleniIme(source.getZaposleni().getIme());
		dto.setZaposleniPrezime(source.getZaposleni().getPrezime());
		dto.setTipId(source.getTip().getId());
		dto.setTipNaziv(source.getTip().getNaziv());
		
		return dto;
	}
	
	public List<ZadatakDTO> convert(List<Zadatak> zadaci){
		List<ZadatakDTO> ret = new ArrayList<>();
		
		for(Zadatak z : zadaci){
			ret.add(convert(z));
		}
		
		return ret;
	}

}
