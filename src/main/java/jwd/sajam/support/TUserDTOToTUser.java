package jwd.sajam.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.sajam.model.TUser;
import jwd.sajam.service.UserService;
import jwd.sajam.web.dto.TUserDTO;

@Component
public class TUserDTOToTUser implements Converter<TUserDTO, TUser> {

	@Autowired
	private UserService userService;
	
	@Override
	public TUser convert(TUserDTO source) {
		TUser user;
		
		if(source.getUsername()==null){
			user = new TUser();
		}else{
			user = userService.findByUsernameAndPassword(source.getUsername(), source.getPassword());
		}
		
		return user;
	}

}
