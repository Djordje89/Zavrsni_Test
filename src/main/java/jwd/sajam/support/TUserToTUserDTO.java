package jwd.sajam.support;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.sajam.model.TUser;
import jwd.sajam.web.dto.TUserDTO;

@Component
public class TUserToTUserDTO implements Converter<TUser, TUserDTO> {

	@Override
	public TUserDTO convert(TUser user) {
	
		TUserDTO userDTO = new TUserDTO();
		userDTO.setUsername(user.getUsername());
		userDTO.setPassword(user.getPassword());
		return userDTO;
	}

}
