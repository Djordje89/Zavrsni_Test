package jwd.sajam.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.sajam.model.Tip;
import jwd.sajam.web.dto.TipDTO;

@Component
public class TipToTipDTO implements Converter<Tip, TipDTO> {

	@Override
	public TipDTO convert(Tip source) {
		
		TipDTO dto = new TipDTO();
		dto.setId(source.getId());
		dto.setNaziv(source.getNaziv());
		
		return dto;
	}

	public List<TipDTO> convert(List<Tip> tipovi) {
		List<TipDTO> ret = new ArrayList<>();
		
		for(Tip t: tipovi){
			ret.add(convert(t));
		}
		
		return ret;
	}
	
}
