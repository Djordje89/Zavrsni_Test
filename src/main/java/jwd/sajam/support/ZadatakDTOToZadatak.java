package jwd.sajam.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.sajam.model.Zadatak;
import jwd.sajam.service.TipService;
import jwd.sajam.service.ZadatakService;
import jwd.sajam.service.ZaposleniService;
import jwd.sajam.web.dto.ZadatakDTO;

@Component
public class ZadatakDTOToZadatak implements Converter<ZadatakDTO, Zadatak> {

	@Autowired
	private ZadatakService zadatakService;
	@Autowired
	private ZaposleniService zaposleniService;
	@Autowired
	private TipService tipService;
	
	@Override
	public Zadatak convert(ZadatakDTO source) {
		
		Zadatak zadatak;
		if(source.getId()==null){
			zadatak = new Zadatak();
			zadatak.setZaposleni(
					zaposleniService.findOne(
							source.getZaposleniId()));
			zadatak.setTip(tipService.findOne(source.getTipId()));
		}else{
			zadatak = zadatakService.findOne(source.getId());
			zadatak.setZaposleni(
					zaposleniService.findOne(
							source.getZaposleniId()));
			zadatak.setTip(tipService.findOne(source.getTipId()));
		}
		zadatak.setNaziv(source.getNaziv());
		zadatak.setOpis(source.getOpis());
		zadatak.setPrioritet(source.getPrioritet());
		zadatak.setRokZavrsetka(source.getRokZavrsetka());
		
		return zadatak;
	}

}
