package jwd.sajam.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.sajam.model.Zaposleni;
import jwd.sajam.web.dto.ZaposleniDTO;

@Component
public class ZaposleniToZaposleniDTO implements Converter<Zaposleni, ZaposleniDTO> {

	@Override
	public ZaposleniDTO convert(Zaposleni source) {
		
		ZaposleniDTO dto = new ZaposleniDTO();
		dto.setId(source.getId());
		dto.setIme(source.getIme());
		dto.setPrezime(source.getPrezime());
		dto.setPozicija(source.getPozicija());
		dto.setBrojDodeljenihZadataka(source.getBrojDodeljenihZadataka());
		
		
		return dto;
	}
	
	public List<ZaposleniDTO> convert(List<Zaposleni> zaposleni) {
		List<ZaposleniDTO> ret = new ArrayList<>();
		
		for(Zaposleni z: zaposleni){
			ret.add(convert(z));
		}
		
		return ret;
	}

}
