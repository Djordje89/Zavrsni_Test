package jwd.sajam.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="zaposleni")
public class Zaposleni {

	@Id
	@GeneratedValue
	@Column
	private Long id;
	@Column
	private String ime;
	@Column
	private String prezime;
	@Column
	private String pozicija;
	@Column
	private int brojDodeljenihZadataka;
	
	@OneToMany(fetch=FetchType.LAZY,cascade=CascadeType.ALL, mappedBy="zaposleni")
	private List<Zadatak> zadaci = new ArrayList<>();
	
	public Long getId() {
		return id;
	}
	
	
	public String getIme() {
		return ime;
	}


	public void setIme(String ime) {
		this.ime = ime;
	}


	public String getPrezime() {
		return prezime;
	}


	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}


	public String getPozicija() {
		return pozicija;
	}


	public void setPozicija(String pozicija) {
		this.pozicija = pozicija;
	}


	public int getBrojDodeljenihZadataka() {
		return brojDodeljenihZadataka;
	}


	public void setBrojDodeljenihZadataka(int brojDodeljenihZadataka) {
		this.brojDodeljenihZadataka = brojDodeljenihZadataka;
	}


	public List<Zadatak> getZadaci() {
		return zadaci;
	}


	public void setZadaci(List<Zadatak> zadaci) {
		this.zadaci = zadaci;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public void addZadatak(Zadatak zadatak){
		this.zadaci.add(zadatak);
		
		if(!this.equals(zadatak.getZaposleni())){
			zadatak.setZaposleni(this);
		}
	}
	
	
	
}
