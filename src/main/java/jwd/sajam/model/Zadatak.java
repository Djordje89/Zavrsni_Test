package jwd.sajam.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table
public class Zadatak {

	
	@Id
	@GeneratedValue
	@Column
	private Long id;
	@Column
	private String naziv;
	@Column
	private String opis;
	@Column
	private int prioritet;
	@Column
	private String rokZavrsetka;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "zaposleni_id", nullable = false)
	private Zaposleni zaposleni;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "tip_id" , nullable = false)
	private Tip tip;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public int getPrioritet() {
		return prioritet;
	}

	public void setPrioritet(int prioritet) {
		this.prioritet = prioritet;
	}

	public String getRokZavrsetka() {
		return rokZavrsetka;
	}

	public void setRokZavrsetka(String rokZavrsetka) {
		this.rokZavrsetka = rokZavrsetka;
	}

	public Zaposleni getZaposleni() {
		return zaposleni;
	}

	public void setZaposleni(Zaposleni zaposleni) {
		this.zaposleni = zaposleni;
	}

	public Tip getTip() {
		return tip;
	}

	public void setTip(Tip tip) {
		this.tip = tip;
	}
	
	
	
	
}
